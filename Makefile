CC = gcc -Wall -c

bin/pscopy: obj/Main.o
	gcc $^ -o $@ -lm

obj/Main.o: src/Main.c
	$(CC) $^ -o $@

.PHONY: clean
clean:
	rm bin/pscopy obj/*.o
