#include <stdio.h>

#include<fcntl.h>
#include<unistd.h>
#include<string.h>
#include<stdlib.h>
#include<sys/stat.h>

int TAMANIO = 200;


int main(int argc, char **argv){
    if (!(argc==3)){
        printf("\n Necesitas dos rutas de archivo \n");   
        printf(" como argumento \n");   
        return 0;
    } //fin de validar 2 argumentos , o bueno 3 contando el nombre de archivo

    umask(0); //en cero como se pide
    int fileA = open(argv[1], O_RDONLY);
    if (fileA<0){ //error
        printf("No se pudo abrir: %s\n", argv[1]);
    }

    int fileB = open(argv[2], O_WRONLY|O_CREAT|O_TRUNC, 0666);
    if (fileB<0){ //error
        printf("No se pudo crear: %s\n", argv[2]);
    }
    
    unsigned char *buffer = malloc(sizeof(unsigned char)*TAMANIO);
    memset(buffer,0,0);
    ssize_t leer = read(fileA,buffer,TAMANIO);

    while (leer!=0){ //lee hasta que ya sea cuando sea cero ya no hay mas que leer
        write(fileB,buffer,TAMANIO);
        memset(buffer,0,TAMANIO);//set size again
        leer = read(fileA,buffer,TAMANIO);
    }

    free(buffer);
    close(fileA);
    close(fileB);
    printf("\n Saliendo...\n");
    return 0;





}